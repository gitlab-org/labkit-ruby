# frozen_string_literal: true

module Labkit
  module Tracing
    # GRPCInterceptor is the deprecated name for GRPCClientInterceptor
    GRPCInterceptor = GRPC::ClientInterceptor
  end
end
