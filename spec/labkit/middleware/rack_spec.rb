# frozen_string_literal: true

describe Labkit::Middleware::Rack do
  let(:app) { double("app") }
  let(:correlation_id) { "the id" }
  let(:metadata) { { "feature_category" => "issue_tracking" } }
  let(:header_metadata) { metadata.merge({ "version" => "1", Labkit::Context::CORRELATION_ID_KEY => correlation_id }) }
  let(:env) { {} }
  let(:fake_request) { double("request") }

  before do
    allow(ActionDispatch::Request).to receive(:new).with(env).and_return(fake_request)
    allow(fake_request).to receive(:request_id).and_return(correlation_id)
  end

  describe "#call" do
    it "adds the correlation id from the request to the context" do
      expect(Labkit::Context).to receive(:with_context).with(a_hash_including(Labkit::Context::CORRELATION_ID_KEY => correlation_id))

      described_class.new(app).call(env)
    end

    it "calls the app" do
      expect(app).to receive(:call).with(env).and_return([nil, {}, nil])

      described_class.new(app).call(env)
    end

    it "injects meta headers" do
      Labkit::Context.push(metadata)

      expect(app).to receive(:call).with(env).and_return([nil, {}, nil])

      _, headers, _ = described_class.new(app).call(env)

      expect(JSON.parse(headers["X-Gitlab-Meta"])).to eq(header_metadata)
    end
  end
end
