require_relative "../../../../support/tracing/shared_examples"

describe Labkit::Tracing::Rails::ActiveSupport::CacheFetchHitInstrumenter do
  where(:key) do
    [
      [nil],
      [123],
    ]
  end

  with_them do
    it_behaves_like "a tracing instrumenter" do
      let(:expected_span_name) { "cache_fetch_hit" }
      let(:payload) { { key: key } }
      let(:expected_tags) do
        {
          "component" => "ActiveSupport",
          "cache.key" => key,
        }
      end
    end
  end
end
