# frozen_string_literal: true

describe Labkit::Tracing::Rails::ActiveSupport::Subscriber do
  using RSpec::Parameterized::TableSyntax

  describe ".instrument" do
    it "is unsubscribeable" do
      unsubscribe = described_class.instrument

      expect(unsubscribe).not_to be_nil
      expect { unsubscribe.call }.not_to raise_error
    end
  end
end
