# frozen_string_literal: true

describe Labkit::Tracing::JaegerFactory do
  describe ".create_tracer" do
    let(:service_name) { "rspec" }

    shared_examples_for "a jaeger tracer" do
      it "responds to active_span methods" do
        expect(tracer).to respond_to(:active_span)
      end

      it "yields control" do
        expect { |b| tracer.start_active_span("operation_name", &b) }.to yield_control
      end
    end

    context "when processing default connections" do
      it_behaves_like "a jaeger tracer" do
        let(:tracer) { described_class.create_tracer(service_name, {}) }
      end
    end

    context "when handling debug options" do
      it_behaves_like "a jaeger tracer" do
        let(:tracer) { described_class.create_tracer(service_name, debug: "1") }
      end
    end

    context "when handling const sampler" do
      it_behaves_like "a jaeger tracer" do
        let(:tracer) { described_class.create_tracer(service_name, sampler: "const", sampler_param: "1") }
      end
    end

    context "when handling probabilistic sampler" do
      it_behaves_like "a jaeger tracer" do
        let(:tracer) { described_class.create_tracer(service_name, sampler: "probabilistic", sampler_param: "0.5") }
      end
    end

    context "when handling http_endpoint configurations" do
      it_behaves_like "a jaeger tracer" do
        let(:tracer) { described_class.create_tracer(service_name, http_endpoint: "http://localhost:1234") }
      end
    end

    context "when handling udp_endpoint configurations" do
      it_behaves_like "a jaeger tracer" do
        let(:tracer) { described_class.create_tracer(service_name, udp_endpoint: "localhost:4321") }
      end
    end

    context "when encountering invalid parameters" do
      it_behaves_like "a jaeger tracer" do
        let(:tracer) { described_class.create_tracer(service_name, invalid: "true") }
      end
    end

    context "when the debug and strict_parser are both set" do
      it_behaves_like "a jaeger tracer" do
        let(:tracer) { described_class.create_tracer(service_name, debug: "1", strict_parsing: "1") }
      end
    end

    it "rejects invalid parameters when strict_parser is set" do
      expect { described_class.create_tracer(service_name, invalid: "true", strict_parsing: "1") }.to raise_error(StandardError)
    end

    context "when processing connection with basic auth credentials" do
      it_behaves_like "a jaeger tracer" do
        let(:tracer) do
          described_class.create_tracer(
            service_name,
            debug: "1",
            http_endpoint: "https://foo:bar@observe.gitlab.com",
          )
        end
      end
    end
  end
end
