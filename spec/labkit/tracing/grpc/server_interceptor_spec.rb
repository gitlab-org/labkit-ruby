# frozen_string_literal: true

require "./spec/support/grpc_service"

describe Labkit::Tracing::GRPC::ServerInterceptor do
  describe "running on RpcServer" do
    subject { described_class.new }

    let(:mock_server) { LabkitTest::TestService::MockServer.new }
    let(:client) { mock_server.start(server_interceptors: [subject]) }

    after do
      mock_server.stop
    end

    # Returns an rspec matcher describing the intended shape of the GRPC tags hash
    def grpc_tags_hash_including(method_name, grpc_type)
      tags = {
        "component" => "grpc",
        "span.kind" => "server",
        "grpc.method" => "/labkit_test.TestService/#{method_name}",
        "grpc.type" => grpc_type,
      }

      hash_including(operation_name: "grpc:/labkit_test.TestService/#{method_name}", tags: tags)
    end

    describe "#req_res_method" do
      it "generates instrumentation" do
        expect(Labkit::Tracing::TracingUtils).to receive(:with_tracing).with(grpc_tags_hash_including("ReqResMethod", "unary")).and_call_original

        client.req_res_method(LabkitTest::Msg.new)
      end
    end

    describe "#server_stream_method" do
      it "generates instrumentation" do
        expect(Labkit::Tracing::TracingUtils).to receive(:with_tracing).with(grpc_tags_hash_including("ServerStreamMethod", "server_stream")).and_call_original

        enumerator = client.server_stream_method(LabkitTest::Msg.new)
        enumerator.each { } # Consume the stream
      end
    end

    describe "#client_stream_method" do
      it "generates instrumentation" do
        expect(Labkit::Tracing::TracingUtils).to receive(:with_tracing).with(grpc_tags_hash_including("ClientStreamMethod", "client_stream")).and_call_original

        client.client_stream_method([LabkitTest::Msg.new])
      end
    end

    describe "#bidi_stream_method" do
      it "generates instrumentation" do
        expect(Labkit::Tracing::TracingUtils).to receive(:with_tracing).with(grpc_tags_hash_including("BidiStreamMethod", "bidi_stream")).and_call_original

        enumerator = client.bidi_stream_method([LabkitTest::Msg.new])
        enumerator.each { } # Consume the stream
      end
    end
  end
end
