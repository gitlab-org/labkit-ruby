# frozen_string_literal: true

require "./spec/support/grpc_service"

describe Labkit::Tracing::GRPC::ClientInterceptor do
  shared_examples_for "a grpc interceptor method" do
    let(:custom_error) { Class.new(StandardError) }

    it "yields" do
      expect { |b| method.call(**kwargs, &b) }.to yield_control
    end

    it "propagates exceptions" do
      expect { method.call(**kwargs) { raise custom_error } }.to raise_error(custom_error)
    end
  end

  shared_examples_for "a client interceptor" do
    describe "#request_response" do
      let(:method) { subject.method(:request_response) }
      let(:kwargs) { { request: {}, call: {}, method: "grc_method", metadata: {} } }

      it_behaves_like "a grpc interceptor method"
    end

    describe "#client_streamer" do
      let(:method) { subject.method(:client_streamer) }
      let(:kwargs) { { requests: [], call: {}, method: "grc_method", metadata: {} } }

      it_behaves_like "a grpc interceptor method"
    end

    describe "#server_streamer" do
      let(:method) { subject.method(:server_streamer) }
      let(:kwargs) { { request: {}, call: {}, method: "grc_method", metadata: {} } }

      it_behaves_like "a grpc interceptor method"
    end

    describe "#bidi_streamer" do
      let(:method) { subject.method(:bidi_streamer) }
      let(:kwargs) { { requests: [], call: {}, method: "grc_method", metadata: {} } }

      it_behaves_like "a grpc interceptor method"
    end
  end

  describe "Labkit::Tracing::GRPC::ClientInterceptor" do
    subject { described_class.instance }

    it_behaves_like "a client interceptor"
  end

  describe "Labkit::Tracing::GRPCInterceptor" do
    subject { Labkit::Tracing::GRPCInterceptor.instance }

    it_behaves_like "a client interceptor"
  end

  describe "running on RpcServer" do
    let(:mock_server) { LabkitTest::TestService::MockServer.new }
    let(:client) { mock_server.start(client_interceptors: [described_class.instance]) }

    after do
      mock_server.stop
    end

    # Returns an rspec matcher describing the intended shape of the GRPC tags hash
    def grpc_tags_hash_including(method_name, grpc_type)
      tags = {
        "component" => "grpc",
        "span.kind" => "client",
        "grpc.method" => "/labkit_test.TestService/#{method_name}",
        "grpc.type" => grpc_type,
      }

      hash_including(operation_name: "grpc:/labkit_test.TestService/#{method_name}", tags: tags)
    end

    describe "#req_res_method" do
      it "generates instrumentation" do
        expect(Labkit::Tracing::TracingUtils).to receive(:with_tracing).with(grpc_tags_hash_including("ReqResMethod", "unary")).and_call_original

        client.req_res_method(LabkitTest::Msg.new)
      end
    end

    describe "#server_stream_method" do
      it "generates instrumentation" do
        expect(Labkit::Tracing::TracingUtils).to receive(:with_tracing).with(grpc_tags_hash_including("ServerStreamMethod", "server_stream")).and_call_original

        client.server_stream_method(LabkitTest::Msg.new)
      end
    end

    describe "#client_stream_method" do
      it "generates instrumentation" do
        expect(Labkit::Tracing::TracingUtils).to receive(:with_tracing).with(grpc_tags_hash_including("ClientStreamMethod", "client_stream")).and_call_original

        client.client_stream_method([LabkitTest::Msg.new])
      end
    end

    describe "#bidi_stream_method" do
      it "generates instrumentation" do
        expect(Labkit::Tracing::TracingUtils).to receive(:with_tracing).with(grpc_tags_hash_including("BidiStreamMethod", "bidi_stream")).and_call_original

        client.bidi_stream_method([LabkitTest::Msg.new])
      end
    end
  end
end
