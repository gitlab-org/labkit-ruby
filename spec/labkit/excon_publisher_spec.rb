# frozen_string_literal: true

require "webrick"
require "rack"
require "excon"

require_relative "../support/test_rack_server"

describe Labkit::ExconPublisher do
  let(:rack_server) { TestRackServer.new(handler_proc) }
  before do
    described_class.labkit_prepend!
    rack_server.start
  end

  after do
    rack_server.stop
  end

  let(:successful_handler_proc) do
    ->(_req) {
      [
        200,
        { "Content-Type" => "application/json" },
        ['{"calculation" => 123}'],
      ]
    }
  end

  let(:successful_response) do
    {
      method: "GET", code: "200",
      scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil,
    }
  end

  let(:bad_request_handler_proc) do
    ->(_req) {
      [
        400,
        { "Content-Type" => "application/json" },
        ['{"error" => "calculation field is required"}'],
      ]
    }
  end

  let(:bad_request_response) do
    {
      method: "GET", code: "400",
      scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil,
    }
  end

  let(:server_error_handler) do
    ->(_req) { raise "something goes wrong" }
  end

  describe "requests made by Excon broadcast request.external_http events" do
    where(:request_proc, :handler_proc, :payloads) do
      [
        [
          -> { Excon.get("http://127.0.0.1:9202/api/v1/tests") },
          successful_handler_proc, [successful_response],
        ],
        [
          -> {
            connection = Excon.new("http://127.0.0.1:9202/api/v1/tests")
            connection.get
            connection.get
          },
          successful_handler_proc, [successful_response, successful_response],
        ],
        [
          -> {
            connection = Excon.new("http://127.0.0.1:9202/api/v1/tests", persistent: true)
            connection.get
            connection.get
          },
          successful_handler_proc, [successful_response, successful_response],
        ],
        [
          -> { Excon.get("http://127.0.0.1:9202/api/v1/tests") },
          bad_request_handler_proc, [bad_request_response],
        ],
        [
          -> {
            connection = Excon.new("http://127.0.0.1:9202/api/v1/tests")
            connection.get
            connection.get
          },
          bad_request_handler_proc, [bad_request_response, bad_request_response],
        ],
        [
          -> {
            connection = Excon.new("http://127.0.0.1:9202/api/v1/tests", persistent: true)
            connection.get
            connection.get
          },
          bad_request_handler_proc, [bad_request_response, bad_request_response],
        ],
        [
          -> {
            connection = Excon.new(
              "http://127.0.0.1:9202/api/v1/tests",
              user: "username", password: "sensitvepassword",
            )
            connection.get
          },
          successful_handler_proc, [successful_response],
        ],
        [
          -> { Excon.get("http://username:sensitvepassword@127.0.0.1:9202/api/v1/tests?abc=1#xyz") },
          successful_handler_proc,
          [
            {
              method: "GET", code: "200",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: "abc=1",
            },
          ],
        ],
        [
          -> { Excon.get("http://127.0.0.1:9202/api/v1/tests", query: { "abc" => "1", xyz: 2, nil_key: nil }) },
          successful_handler_proc,
          [
            {
              method: "GET", code: "200",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: "abc=1&nil_key=&xyz=2",
            },
          ],
        ],
        [
          -> { Excon.get("http://127.0.0.1:9202/this/is/a/not/found/path") },
          successful_handler_proc,
          [
            {
              method: "GET", code: "404",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/this/is/a/not/found/path", query: nil,
            },
          ],
        ],
        [
          -> { Excon.post("http://127.0.0.1:9202/api/v1/tests", body: '{ "a" => 1 }') },
          ->(_req) { [204, {}, [""]] },
          [
            {
              method: "POST", code: "204",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil,
            },
          ],
        ],
        [
          -> { Excon.head("http://127.0.0.1:9202/api/v1/tests") },
          ->(_req) { [204, {}, [""]] },
          [
            {
              method: "HEAD", code: "204",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil,
            },
          ],
        ],
        [
          -> {
            connection = Excon.new("http://127.0.0.1:9202/api/v1/tests", proxy: "http://127.0.0.1:9202")
            connection.get
          },
          successful_handler_proc,
          [
            {
              method: "GET", code: "200",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil,
              proxy_host: "127.0.0.1", proxy_port: 9202,
            },
          ],
        ],
        [
          -> {
            connection = Excon.new("http://127.0.0.1:9202/api/v1/tests")
            connection.get(query: { "abc" => "1", xyz: 2 })
          },
          successful_handler_proc,
          [
            {
              method: "GET", code: "200",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: "abc=1&xyz=2",
            },
          ],
        ],
      ]
    end

    with_them do
      it "broadcasts a correct notification" do
        broadcasted = false
        begin
          subscriber = ActiveSupport::Notifications.subscribe("request.external_http") do |*args|
            broadcasted = true
            event = ActiveSupport::Notifications::Event.new(*args)
            expect(event.name).to eq("request.external_http")
            expected_layload = payloads.shift # The shift here is to validate the number of notifications broadcasted as well
            duration = event.payload.delete(:duration)
            expect(duration).to be_a(Float).and(be > 0.0)
            expect(event.payload).to eql(expected_layload)
          end
          request_proc.call
        ensure
          ActiveSupport::Notifications.unsubscribe(subscriber) if subscriber
        end
        expect(broadcasted).to be(true)
      end
    end
  end

  describe "failed requests made by Excon broadcast request.external_http events" do
    context "with request timeout" do
      let(:handler_proc) do
        ->(_req) {
          sleep 0.1
          [
            200,
            { "Content-Type" => "application/json" },
            ['{"calculation" => 123}'],
          ]
        }
      end

      it "broadcasts an exception notification" do
        broadcasted = false
        begin
          subscriber = ActiveSupport::Notifications.subscribe("request.external_http") do |*args|
            broadcasted = true
            event = ActiveSupport::Notifications::Event.new(*args)
            expect(event.name).to eq("request.external_http")
            duration = event.payload.delete(:duration)
            expect(duration).to be_a(Float).and(be > 0.0)
            expect(event.payload).to include(
              method: "GET",
              scheme: "http", host: "127.0.0.1", port: 9202, path: "/api/v1/tests", query: nil,
              exception: ["Excon::Error::Timeout", "read timeout reached"],
              exception_object: be_a(Excon::Error::Timeout),
            )
          end
          Excon.get("http://127.0.0.1:9202/api/v1/tests", read_timeout: 0.01)
        rescue Excon::Error::Timeout
          # Ignored
        ensure
          ActiveSupport::Notifications.unsubscribe(subscriber) if subscriber
        end
        expect(broadcasted).to be(true)
      end
    end
  end
end
