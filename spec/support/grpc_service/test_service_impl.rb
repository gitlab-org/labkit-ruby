# frozen_string_literal: true

require_relative "test_services_pb"

module LabkitTest
  module TestService
    class TestServerImpl < LabkitTest::TestService::Service
      def req_res_method(req, _)
        code = req.error_code
        raise ::GRPC::BadStatus.new_status_exception(code, "test exception") if code > 0
        raise code.to_s if code < 0

        LabkitTest::Msg.new(name: "response")
      end

      def server_stream_method(_, _)
        [LabkitTest::Msg.new(name: "response")]
      end

      def client_stream_method(call)
        call.each_remote_read { |msg| }

        LabkitTest::Msg.new(name: "response")
      end

      def bidi_stream_method(call, view)
        bidi_stream_method_shadow(call, view)
      end

      # We can safely stub this method with rspec
      #  If we attempt to stub `bidi_stream_method` GRPC fails
      # with a `Illegal arity of reply generator` error,
      # since GRPC will introspect the stub and return
      # the wrong arity for the function. This is a workaround
      def bidi_stream_method_shadow(call, _view)
        call.each { |msg| }

        [LabkitTest::Msg.new(name: "response")]
      end
    end
  end
end
